import java.util.Scanner;

public class App {

    /**
     * @param args unused unused
     * @throws Exception unused
     */
    // The main function of the program.
    /**
     * The above function takes 4 integers as input and checks if the sum of the 4
     * integers is
     * divisible by 5. If it is, it prints "Berhenti", otherwise it prints "Jalan".
     */
    public static void main(String[] args) throws Exception {
        Scanner kScanner = new Scanner(System.in);
        String RawInput = "" + kScanner.nextInt() + kScanner.nextInt() + kScanner.nextInt() + kScanner.nextInt();
        if (Long.valueOf(RawInput) % 5 == 0) {
            System.out.println("Berhenti");
        } else {
            System.out.println("Jalan");
        }
        kScanner.close();
    }
}
